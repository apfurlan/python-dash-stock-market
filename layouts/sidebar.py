# the style arguments for the sidebar. We use position:fixed and a fixed width
sidebar_style = {
    "position": "fixed",
    "top": 0,
    "left": 3,
    "bottom": 0,
    "width": "18rem",
    "padding": "4rem 1rem",
    "background-color": "#e9ecef",
}