import dash

import dash_bootstrap_components as dbc
from dash import Input, Output,dcc,html,dash_table

import yfinance as yf
import investpy

# from layouts.sidebar    import sidebar_style
# from layouts.contentbar import content_style
# from frontend.components.generate_table import generate_table
from frontend.components.generate_candle_plot import candlestick_plot
from frontend.components.generate_candle_plot import histogram_plot
from frontend.components.generate_table import generate_table

import pandas as pd
import numpy as np
import json

import fundamentus as fdmt
import matplotlib.pyplot as plt

# with open('home_text.md', 'r') as file:
#     home_md_text = file.read()



# # ============= fig 1 ============================ 
#df = px.data.gapminder().query("country=='Canada'")
# fig = px.line(df, x="year", y="lifeExp", title='Life expectancy in Canada')
# fig.update_layout(autosize=False,
#                   width=600,
#                   height=500)
#fig.show()
 
## ============ fig 2  ===============================

#def candlestick_plot(ticker="PETR4.SA") :

ticker="PETR4.SA"
data = yf.download(ticker, period="5mo", interval="1d")


# # ================================================
# # Layout
# # ===============================================

layout = dbc.Container([
    
    html.H1("Análise Técnica", style={'textAlign':'center'}),
    
    dbc.Row([
        dbc.Col([
            dcc.Graph(
                id="candlestick"
                , figure = candlestick_plot(data,ticker=ticker)
                , style={'width': '30vh', 'height': '30vh'}) 
            ]),
        dbc.Col([
            dash_table.DataTable(
                data.to_dict('records')[:5],[{"name": i, "id": i} for i in ['Close','Adj Close']])
            ])
    ]),
    dbc.Row(
            [
            dbc.Col([
                dcc.Graph(
                    id="volume"
                    , figure = histogram_plot(data,ticker=ticker)
                    , style={'width': '30vh', 'height': '30vh'}) 
                ])
            ]
        ),
    # row_drop := dcc.Dropdown(value=10
    #                          ,clearable=False
    #                          ,style={'width':'35%'}
    #                          ,options=[10,25,50,100]
    #                 ),
    
    # my_table := dash_table.DataTable(
    #     data = df.to_dict('records'),
    #     filter_action='native',
    #     page_size=10,

    #     style_data = {
    #         'width' : '150px',
    #         'minWidth' : '150px',
    #         'maxWidth' : '150px',
    #         'overflow': 'hidden',
    #         'textOverflow' : 'ellipsis',
    #     }
    # ),

    # dbc.Row([
    #     dbc.Col([
    #         continent_drop := dcc.Dropdown([ x for x in sorted(df.papel.unique())])
    #     ])
    # ])

])

app = dash.Dash(__name__,external_stylesheets=[dbc.themes.BOOTSTRAP])
app.layout = layout 

# if __name__ == "__main__":
#     app.run_server(debug=True)
    # html.Div([dcc.Location(id="url"),
    #     dash_table.DataTable(
    #     id='data_table',
    #     columns=[{
    #         'name': 'Column {}'.format(i),
    #         'id': 'column-{}'.format(i),
    #     } for i in range(1, 5)],
    #     data=[
    #     {'column-{}'.format(i): (j + (i-1)*5) for i in range(1, 5)}
    #     for j in range(5)
    #     ]
    # ),
    # html.Div(id='output_div')
    # ])

    #html.Div([dcc.Location(id="url"),
        # dbc.Row([
        #     dbc.Col([
        #         sidebar
        #     ]),
        #     dbc.Col([
        #         dcc.Graph(
        #             id="first-graph"
        #             , figure=fig
        #             , style={'width': '50vh', 'height': '50vh'})
        #         ]),
            # dbc.Col([
            #     dcc.Graph(
            #         id="second-graph"
            #         , figure=fig2
            #         , style={'width': '30vh', 'height': '30vh'}) 
            # ])
        #])
    #])


#app.layout = html.Div([dcc.Location(id="url"), sidebar, content])

# @app.callback(Output("page-content", "children"), 
#               [Input("url", "pathname")],
#               prevent_initial_call=True)

# def render_page_content(pathname):
#     if pathname == "/":
#         #return html.P("This is the content of the home page!")

#         path = "/home/apfurlan/python-dash-stock-market/pages/home/home_text.md"
#         read = open(path, 'r').read()
#         return dcc.Markdown(children=read)
    
#     elif pathname == "/page-1":
 
#         return html.P("Oh cool, this is page 1!")
#         #return df.head(10)
#         #generate_table(df)
#     elif pathname == "/page-2":
        
#         @app.callback(
#             Output("table-wrapper","children") , 
#             Input("date-slider","value"),
#             prevent_initial_call=True
#         )
#         def update_table(selected_year) : 
#             filtered_df = df[df.index.strftime('%Y') == '2022']
#             tab = dash_table.DataTable(
#                 id="table",
#                 columns=[{"name": i, "id": i} for i in filtered_df.columns],
#                 data = filtered_df.to_dict('records')
#             )

#         df = yf.download("PETR4.SA", period="3y", interval="1d")

#         return (
#             dcc.Slider(
#                   id='date-slider'
#                 , min = df.index.min()
#                 , max = df.index.max()
#                 #, value=df.index.min()
#                 , marks={str(year) : str(year) 
#                          for year in df.index.strftime('%Y').unique()}
#                 , step=None
#             )
        
#             # html.Div(
#             #       id = "table-wrapper"  
#             #     , style = {"width" : "auto" , "overflow-y" : "scroll"}
#             #     , children = [dash_table.DataTable(
#             #           id="table"
#             #         , columns = [{"name": i, "id": i} for i in df.columns]
#             #         , data = df.to_dict('records')
#             #         )
#             #     ]
#             # )
#         )
    
#     elif pathname == "/page-3":
#         return html.P("Aprendndo a criar dashs no python")
#     # If the user tries to reach a different page, return a 404 message
#     return html.Div(
#         [
#             html.H1("404: Not found", className="text-danger"),
#             html.Hr(),
#             html.P(f"The pathname {pathname} was not recognised..."),
#         ],
#         className="p-3 bg-light rounded-3",
#     )


# if __name__ == '__main__':
#     app.run_server(debug=True)

