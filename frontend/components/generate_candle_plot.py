import plotly.express as px
import plotly.graph_objects as go

def candlestick_plot(df,ticker="PETR4.SA") :
    fig = go.Figure(
    data=[
        go.Candlestick(
            x      = df.index
            , open = df['Open'] 
            , high = df['High']
            , low  = df['Low'] 
            , close= df['Close']
        )
    ])

    fig.update_layout(xaxis_rangeslider_visible=False,
        #title=f"{ticker}",
        yaxis_title='Preço',
        autosize=False,
        width=700,
        height=400
    )


    return fig

def histogram_plot(df,ticker="PETR4.SA") : 

    fig = px.bar(df, x=df.index, y='Volume')
    
    fig.update_layout(xaxis_rangeslider_visible=False,
        #title=f"{ticker}",
        yaxis_title='Volume',
        autosize=False,
        width=700,
        height=400
    )

    return fig