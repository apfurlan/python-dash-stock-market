import dash

import dash_bootstrap_components as dbc
from dash import Input, Output,dcc,html,dash_table


sidebar = html.Div([
        
    html.Img(
        id="logo"
        #, src=app.get_asset_url("")
        , height = 100
        , width  = 160
    ),
    html.P(),
    html.H2("Stock Market"
            , className="display-5"
            , style={"font-weight": "bold"}
    )
    , html.Hr()
    , html.P(
        "A platform to simplify the Bazillian Stock Market visualization" 
        , className="lead"
    ),
    dbc.Nav(
        [
            dbc.NavLink("Home",       href="/",       active="exact"),
            dbc.NavLink("Wallet",     href="/page-1", active="exact"),
            dbc.NavLink("EY",    href="/page-2", active="exact"),
            dbc.NavLink("Comodities", href="/page-3", active="exact"),
        ],
        vertical=True,
        pills=True,
    ),
    ],
    #style=sidebar_style,
)
